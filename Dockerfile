FROM alpine:3.17
WORKDIR /
COPY /bin/manager .
COPY gin-webserver-chart-0.1.0.tgz /tmp/
USER 65532:65532

ENTRYPOINT ["/manager"]